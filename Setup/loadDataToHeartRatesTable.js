const AWS = require("aws-sdk");
const fs = require('fs');

AWS.config.update({
    region: "ap-southeast-1",
    endpoint: "dynamodb.ap-southeast-1.amazonaws.com"
});

let docClient = new AWS.DynamoDB.DocumentClient();

console.log("Importing patients' heart rate into DynamoDB. Please wait.");

let allHeartRates = JSON.parse(fs.readFileSync('sample-heart-beat.json', 'utf8'));
allHeartRates.forEach(heartRate => {
    let params = {
        TableName: "HeartRates",
        Item: {
            "id": "51864875-ca29-4bb2-9713-d1aca0deb2ce",
            "recorded_date": heartRate.recorded_date,
            "heartRate": heartRate.heartbeat
        }
    }
    docClient.put(params, function (err, data) {
        if (err) {
            console.error("Unable to add heart rate. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("PutItem succeeded:");
        }
    });
})
