const AWS = require("aws-sdk");

AWS.config.update({
    region: "ap-southeast-1",
    endpoint: "dynamodb.ap-southeast-1.amazonaws.com"
});

let dynamodb = new AWS.DynamoDB();

let params = {
    TableName: "HeartRates",
    KeySchema: [
        {AttributeName: "id", KeyType: "HASH"},
        {AttributeName: "recorded_date", KeyType: "RANGE"}
    ],
    AttributeDefinitions: [
        {AttributeName: "id", AttributeType: "S"},
        {AttributeName: "recorded_date", AttributeType: "S"}
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
    }
};

dynamodb.createTable(params, function (err, data) {
    if (err) {
        console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
    }
});
