let AWS = require("aws-sdk");
let fs = require('fs');

AWS.config.update({
    region: "ap-southeast-1",
    endpoint: "dynamodb.ap-southeast-1.amazonaws.com"
});

let docClient = new AWS.DynamoDB.DocumentClient();

console.log("Importing patients into DynamoDB. Please wait.");

let allPatients = JSON.parse(fs.readFileSync('sample-patients.json', 'utf8'));
allPatients.forEach(function (patient) {
    let params = {
        TableName: "Patients",
        Item: {
            "id": patient.id,
            "data": patient
        }
    };

    docClient.put(params, function (err, data) {
        if (err) {
            console.error("Unable to add patient", patient.title, ". Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("PutItem succeeded:", patient.title);
        }
    });
});